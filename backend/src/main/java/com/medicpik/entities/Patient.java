package com.medicpik.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@DiscriminatorValue("PATIENT")
@Table(name="patient")
public class Patient extends User{

    @Column(unique = true)
    @JsonIgnore
    String pesel;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL)
    @JsonIgnore
    List<Appointment> appointments;

    public Patient(){}
    public Patient(String name, String surname, String login, String password, String pesel) {
        super(name, surname, login, password);
        this.pesel = pesel;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

}
