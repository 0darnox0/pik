package com.medicpik.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="role",discriminatorType=DiscriminatorType.STRING)
@Table(name="user")
public class User {

    @Id
    //@NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@NotNull
    private String first_name;

    //@NotNull
    private String last_name;

    private String login;

    @JsonIgnore
    private String password;

    @Column(name = "role", insertable = false, updatable = false)
    private String role;

    public User(){}
    public User(String first_name, String last_name, String login, String password) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.login = login;
        setPassword(password);
    }

    public Long getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirst_name(String name) {
        this.first_name = name;
    }

    public void setLast_name(String surname) {
        this.last_name = surname;
    }

    public String getLogin() {
        return login;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = DigestUtils.sha256Hex(password);
    }

    public String getRole() {
        return role;
    }
}
