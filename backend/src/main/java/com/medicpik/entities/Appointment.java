package com.medicpik.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Entity
@Table(name="appointment")
public class Appointment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    Patient patient;

    @ManyToOne(cascade = CascadeType.MERGE)
    Doctor doctor;

    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime date_start;

    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime date_end;

    private String room;

    private boolean canceled;


    public Appointment(){}

    public Appointment(Patient patient, Doctor doctor, OffsetDateTime date_start, OffsetDateTime date_end, String room, boolean canceled) {
        this.patient = patient;
        this.doctor = doctor;
        this.date_start = date_start;
        this.date_end = date_end;
        this.room = room;
        this.canceled = canceled;
    }

    public OffsetDateTime getDate_start() {
        return date_start;
    }

    public OffsetDateTime getDate_end() {
        return date_end;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public void setDate_start(OffsetDateTime startDate) {
        this.date_start = startDate;
    }

    public void setDate_end(OffsetDateTime endDate) {
        this.date_end = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Long getId() {
        return id;
    }

    public String getRoom() {
        return room;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }
}
