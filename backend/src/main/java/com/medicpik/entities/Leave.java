package com.medicpik.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "_leave")
public class Leave {

    @Id
    //@NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    //@NotNull
    @ManyToOne(cascade = CascadeType.MERGE)
    Doctor doctor;

    //@NotNull
    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime start_date;

    //@NotNull
    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime end_date;

    public Leave(){}

    public Leave(Doctor doctor, OffsetDateTime start_date, OffsetDateTime end_date) {
        this.doctor = doctor;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public Long getIdAppointment() {
        return id;
    }

    public OffsetDateTime getStart_date() {
        return start_date;
    }

    public OffsetDateTime getEnd_date() {
        return end_date;
    }

    public void setIdAppointment(Long idAppointment) {
        this.id = idAppointment;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public void setStart_date(OffsetDateTime startDate) {
        this.start_date = startDate;
    }

    public void setEnd_date(OffsetDateTime endDate) {
        this.end_date = endDate;
    }

    public Doctor getDoctor() {
        return doctor;
    }
}
