package com.medicpik.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "dailyschedule")
public class DailySchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    int week_day;
    int start_hour; // [0 : 23]
    int start_minutes; // [0 : 59]
    int end_hour; // [0 : 23]
    int end_minutes; // [0 : 59]

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference
    WeeklySchedule weekly_schedule;

    public DailySchedule() {
    }

    public DailySchedule(int week_day, int start_hour, int start_minutes, int end_hour, int end_minutes, WeeklySchedule weekly_schedule) {
        this.week_day = week_day;
        this.start_hour = start_hour;
        this.start_minutes = start_minutes;
        this.end_hour = end_hour;
        this.end_minutes = end_minutes;
        this.weekly_schedule = weekly_schedule;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWeek_day() {
        return week_day;
    }

    public void setWeek_day(int week_day) {
        this.week_day = week_day;
    }

    public int getStart_hour() {
        return start_hour;
    }

    public void setStart_hour(int start_hour) {
        this.start_hour = start_hour;
    }

    public int getStart_minutes() {
        return start_minutes;
    }

    public void setStart_minutes(int start_minutes) {
        this.start_minutes = start_minutes;
    }

    public int getEnd_hour() {
        return end_hour;
    }

    public void setEnd_hour(int end_hour) {
        this.end_hour = end_hour;
    }

    public int getEnd_minutes() {
        return end_minutes;
    }

    public void setEnd_minutes(int end_minutes) {
        this.end_minutes = end_minutes;
    }

    public WeeklySchedule getWeekly_schedule() {
        return weekly_schedule;
    }

    public void setWeekly_schedule(WeeklySchedule weekly_schedule) {
        this.weekly_schedule = weekly_schedule;
    }
}
