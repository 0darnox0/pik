package com.medicpik.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.medicpik.dto.DoctorDTO;
import com.medicpik.dto.OfficeHoursDTO;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Entity
@DiscriminatorValue("DOCTOR")
@Table(name="doctor")
public class Doctor extends User{

    private String specialization;

    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL)
    private List<Leave> leaves;

    @JsonManagedReference
    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<WeeklySchedule> weeklySchedules = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL)
    private List<Appointment> appointments = new ArrayList<>();


    public Doctor(){}
    public Doctor(String name, String surname, String login, String password, String specialization) {
        super(name, surname, login, password);
        this.specialization = specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getSpecialization() {
        return specialization;
    }

    @JsonIgnore
    public List<WeeklySchedule> getWeeklySchedules() {
        return weeklySchedules;
    }

    public void setWeeklySchedules(List<WeeklySchedule> weeklySchedules) {
        this.weeklySchedules = weeklySchedules;
    }

    public String toString(){
        return "< doctor: id: " + getId() + " specialization: " + specialization + ">";
    }

    @JsonIgnore
    public List<Appointment> getAppointments() {
        return appointments;
    }

    @JsonIgnore
    public List<OfficeHoursDTO> getOfficeHours() {
        List<OfficeHoursDTO> officeHours = new LinkedList<>();
        List<WeeklySchedule> doctorsSchedules = getWeeklySchedules();
        List<Appointment> doctorAppointments = getAppointments();

        DoctorDTO doctorDTO = DoctorDTO.fromDoctor(this);

        for (WeeklySchedule schedule : doctorsSchedules) {
            HashMap<Integer, DailySchedule> dayOfWeek2Schedule = new HashMap<>();

            for (DailySchedule ds : schedule.getSchedule()) {
                dayOfWeek2Schedule.put(ds.getWeek_day(), ds);
            }

            for (OffsetDateTime date = schedule.startOfValidity; date.isBefore(schedule.endOfValidity); date = date.plusDays(1)) {
                DailySchedule ds = dayOfWeek2Schedule.get(date.getDayOfWeek().getValue());
                if(ds == null) continue;

                OffsetDateTime startDate = date.withHour(ds.getStart_hour()).withMinute(ds.getStart_minutes());
                OffsetDateTime endDate = date.withHour(ds.getEnd_hour()).withMinute(ds.getEnd_minutes());

                for (OffsetDateTime appDate = startDate; appDate.isBefore(endDate); ) {

                    OffsetDateTime startTime = appDate;
                    appDate = appDate.plusHours(1);
                    OffsetDateTime endTime = appDate;

                    boolean occupied = false;
                    Long id = null;
                    for(Appointment app : doctorAppointments){
                        if(app.getDate_start().equals(startTime) && app.getDate_end().equals(endTime) && !app.isCanceled()){
                            occupied = true;
                            id = app.getId();
                            break;
                        }
                    }

                    officeHours.add(new OfficeHoursDTO(doctorDTO, startTime, endTime, occupied, id));
                }
            }
        }

        return officeHours;
    }
}
