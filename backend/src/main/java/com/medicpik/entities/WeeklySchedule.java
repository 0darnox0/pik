package com.medicpik.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="weeklyschedule")
public class WeeklySchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(cascade = CascadeType.MERGE )
    @JsonBackReference
    Doctor doctor;

    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    @Column(name = "start_of_validity")
    public OffsetDateTime startOfValidity;

    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    @Column(name = "end_of_validity")
    public OffsetDateTime endOfValidity;

    @JsonManagedReference
    @OneToMany(mappedBy = "weekly_schedule", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DailySchedule> schedule = new ArrayList<>();

    public WeeklySchedule() {}

    public WeeklySchedule(Doctor doctor, OffsetDateTime startOfValidity, OffsetDateTime endOfValidity) {
        this.doctor = doctor;
        this.startOfValidity = startOfValidity;
        this.endOfValidity = endOfValidity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public OffsetDateTime getStartOfValidity() {
        return startOfValidity;
    }

    public void setStartOfValidity(OffsetDateTime startOfValidity) {
        this.startOfValidity = startOfValidity;
    }

    public OffsetDateTime getEndOfValidity() {
        return endOfValidity;
    }

    public void setEndOfValidity(OffsetDateTime endOfValidity) {
        this.endOfValidity = endOfValidity;
    }

    public List<DailySchedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<DailySchedule> schedule) {
        this.schedule = schedule;
    }

    public void addDailySchedule(DailySchedule dailySchedule) {
        schedule.add(dailySchedule);
        dailySchedule.setWeekly_schedule(this);
    }

    public void removeDailySchedule(DailySchedule dailySchedule) {
        schedule.remove(dailySchedule);
        dailySchedule.setWeekly_schedule(null);
    }
}
