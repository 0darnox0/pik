package com.medicpik.repositories;

import com.medicpik.entities.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    Optional<Appointment> findById(Long appointment_id);
    List<Appointment> findByPatientId(Long patient_id);
    List<Appointment> findByDoctorId(Long user_id);
    Optional<Appointment> findByIdAndPatientId(Long appointment_id, Long user_id);
    Optional<Appointment> findByIdAndDoctorId(Long appointment_id, Long user_id);

}
