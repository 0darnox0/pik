package com.medicpik.repositories;

import com.medicpik.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserById(Long user_id);
    Optional<User> findByLogin(String login);
    List<User> findAllByLogin(String login);
}
