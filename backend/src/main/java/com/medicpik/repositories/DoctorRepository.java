package com.medicpik.repositories;

import com.medicpik.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    Optional<Doctor> findDoctorById(Long doctor_id);

    @Query("Select d from Doctor d WHERE CONCAT(d.first_name, ' ', d.last_name) like %:name%")
    List<Doctor> searchByName(@Param("name") String name);
}
