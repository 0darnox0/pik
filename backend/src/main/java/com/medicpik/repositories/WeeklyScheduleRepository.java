package com.medicpik.repositories;

import com.medicpik.entities.WeeklySchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WeeklyScheduleRepository extends JpaRepository<WeeklySchedule, Long> {
    Optional<WeeklySchedule> findByIdAndDoctorId(Long schedule_id, Long doctor_id);
    List<WeeklySchedule> findByDoctorId(Long doctor_id);
}
