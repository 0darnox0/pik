package com.medicpik.repositories;

import com.medicpik.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
    Optional<Patient> findPatientById(Long patient_id);
    Optional<Patient> findByPesel(String pesel);
}
