package com.medicpik.resources;

import com.medicpik.dto.WeeklyScheduleDTO;
import com.medicpik.entities.WeeklySchedule;
import com.medicpik.repositories.WeeklyScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("weekly-schedules/")
public class WeeklyScheduleResource {

    @Autowired
    WeeklyScheduleRepository weeklyScheduleRepository;

    @GetMapping(value = "/{schedule_id}", produces = "application/json")
    public WeeklySchedule getWeeklySchedule(@PathVariable Long schedule_id) {
        Optional<WeeklySchedule> weeklySchedule = weeklyScheduleRepository.findById(schedule_id);
        if(!weeklySchedule.isPresent())
            throw new ResourceNotFoundException("Weekly schedule", schedule_id.toString());

        return weeklySchedule.get();
    }
}
