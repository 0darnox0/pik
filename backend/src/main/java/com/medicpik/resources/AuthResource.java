package com.medicpik.resources;

import com.medicpik.dto.LoginFormDTO;
import com.medicpik.dto.LoginResponseDTO;
import com.medicpik.entities.User;
import com.medicpik.repositories.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Random;


@RestController
@RequestMapping("/auth")
public class AuthResource {

    private final UserRepository userRepository;

    @Autowired
    public AuthResource(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity<Object> login(@Valid @RequestBody LoginFormDTO loginFormDTO) {
        User user = userRepository.findByLogin(loginFormDTO.getLogin()).orElseThrow(
                () -> new ResourceNotFoundException("User", loginFormDTO.getLogin()));

        boolean passwordCorrect = checkPassword(user.getPassword(), loginFormDTO.getPassword());
        if (!passwordCorrect)
            return ResponseEntity.badRequest().body("Incorrect username or password.");

        String token = DigestUtils.sha256Hex(String.valueOf(new Random().nextInt()));

        LoginResponseDTO response = LoginResponseDTO.fromUser(user);
        response.setToken(token);

        return ResponseEntity.ok().body(response);
    }

    private boolean checkPassword(String hash, String password) {
        return DigestUtils.sha256Hex(password).equals(hash);
    }
}
