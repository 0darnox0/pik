package com.medicpik.resources;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    private String id;

    public ResourceNotFoundException(String resourceName, String id) {
        super(String.format(resourceName + " not found. Id: '%s'", id));
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

}