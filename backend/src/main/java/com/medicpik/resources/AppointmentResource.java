package com.medicpik.resources;

import com.medicpik.dto.AppointmentDTO;
import com.medicpik.dto.OfficeHoursDTO;
import com.medicpik.entities.Appointment;
import com.medicpik.entities.Doctor;
import com.medicpik.entities.Patient;
import com.medicpik.repositories.AppointmentRepository;
import com.medicpik.repositories.DoctorRepository;
import com.medicpik.repositories.PatientRepository;
import com.medicpik.repositories.WeeklyScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/appointments")
public class AppointmentResource {

    private final AppointmentRepository appointmentRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;


    @Autowired
    public AppointmentResource(AppointmentRepository appointmentRepository, PatientRepository patientRepository, DoctorRepository doctorRepository) {
        this.appointmentRepository = appointmentRepository;
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public AppointmentDTO getAppointment(@PathVariable Long id) {
        Appointment appointment = appointmentRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Appointment", id.toString()));

        return AppointmentDTO.fromAppointment(appointment);
    }

    @GetMapping(value = "", produces = "application/json")
    public List<AppointmentDTO> getAllAppointments() {
        List<Appointment> appointments = appointmentRepository.findAll();

        return appointments.stream().map(app -> AppointmentDTO.fromAppointment(app)).collect(Collectors.toList());
    }

    @PostMapping("")
    public ResponseEntity<Object> createAppointment(@RequestBody AppointmentDTO appointmentDto) {
        Appointment appointment = AppointmentDTO.toAppointment(appointmentDto);
        appointment.setId(null);
        if(!isAppointmentValuable(appointment))
            return ResponseEntity.badRequest().build();

        appointment.setRoom(String.valueOf(new Random().nextInt(60)));

        Appointment savedAppointments = appointmentRepository.save(appointment);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedAppointments.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateAppointment(@RequestBody AppointmentDTO appointmentDto, @PathVariable Long id) {
        Appointment appointment = appointmentRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Appointment", id.toString()));

        Appointment appointmentDTO = AppointmentDTO.toAppointment(appointmentDto);
        appointment.setCanceled(appointmentDTO.isCanceled());

        appointmentRepository.save(appointment);
        return ResponseEntity.noContent().build();
}

    private boolean isAppointmentValuable(Appointment appointment){
        Optional<Patient> patient = patientRepository.findPatientById(appointment.getPatient().getId());
        Optional<Doctor> doctor = doctorRepository.findDoctorById(appointment.getDoctor().getId());

        if(!patient.isPresent() || !doctor.isPresent())
            return false;
        if(!isTimeAvailable(doctor.get().getOfficeHours(), appointment.getDate_start(), appointment.getDate_end()))
            return false;

        return true;
    }

    private boolean  isTimeAvailable(List<OfficeHoursDTO> officeHours, OffsetDateTime date_start, OffsetDateTime date_end){
        for(OfficeHoursDTO officeHour : officeHours){
            if(date_start.isEqual(officeHour.start) && !officeHour.occupied)
                return true;
        }
        return false;
    }

}
