package com.medicpik.resources;

import com.medicpik.dto.DoctorDTO;
import com.medicpik.dto.OfficeHoursDTO;
import com.medicpik.dto.WeeklyScheduleDTO;
import com.medicpik.entities.Appointment;
import com.medicpik.entities.DailySchedule;
import com.medicpik.entities.Doctor;
import com.medicpik.entities.WeeklySchedule;
import com.medicpik.repositories.AppointmentRepository;
import com.medicpik.repositories.DoctorRepository;
import com.medicpik.repositories.WeeklyScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("/doctors")
public class DoctorResource {

    private final DoctorRepository doctorRepository;
    private final WeeklyScheduleRepository weeklyScheduleRepository;
    private final AppointmentRepository appointmentRepository;

    @Autowired
    public DoctorResource(DoctorRepository doctorRepository, WeeklyScheduleRepository weeklyScheduleRepository, AppointmentRepository appointmentRepository) {
        this.doctorRepository = doctorRepository;
        this.weeklyScheduleRepository = weeklyScheduleRepository;
        this.appointmentRepository = appointmentRepository;
    }

    @GetMapping(value = "/{doctor_id}", produces = "application/json")
    public DoctorDTO getDoctor(@PathVariable Long doctor_id) {
        Doctor doctor = doctorRepository.findById(doctor_id).orElseThrow(
                () -> new ResourceNotFoundException("Doctor", doctor_id.toString()));

        return DoctorDTO.fromDoctor(doctor);
    }

    @GetMapping(value = "", produces = "application/json")
    public Stream<DoctorDTO> getDoctors() {
        List<Doctor> doctors = doctorRepository.findAll();
        return doctors.stream().map(doctor -> DoctorDTO.fromDoctor(doctor));
    }

    @GetMapping(value = "/{doctor_id}/office-hours", produces = "application/json")
    public List<OfficeHoursDTO> getOfficeHours(@PathVariable Long doctor_id) {
        Doctor doctor = doctorRepository.findById(doctor_id).orElseThrow(
                () -> new ResourceNotFoundException("Doctor", doctor_id.toString()));

        return doctor.getOfficeHours();
    }

    @GetMapping(value = "/{doctor_id}/weekly-schedules/{schedule_id}", produces = "application/json")
    public WeeklyScheduleDTO getWeeklySchedule(@PathVariable Long doctor_id, @PathVariable Long schedule_id) {
        WeeklySchedule weeklySchedule = weeklyScheduleRepository.findByIdAndDoctorId(schedule_id, doctor_id).
                orElseThrow(() -> new ResourceNotFoundException("Weekly schedule", schedule_id.toString()));

        return WeeklyScheduleDTO.fromWeeklySchedule(weeklySchedule);
    }

    @GetMapping(value = "/{doctor_id}/weekly-schedules", produces = "application/json")
    public List<WeeklySchedule> getWeeklySchedules(@PathVariable Long doctor_id) {
        List<WeeklySchedule> weeklySchedules = weeklyScheduleRepository.findByDoctorId(doctor_id);
        if (weeklySchedules.isEmpty())
            throw new ResourceNotFoundException("Weekly schedule", "doctor: " + doctor_id.toString());

        return weeklySchedules;
    }

    @GetMapping(value = "/search/{searchKeyword}", produces = "application/json")
    public Stream<DoctorDTO> getDoctorsByTerm(@PathVariable String searchKeyword) {
        List<Doctor> doctors = doctorRepository.searchByName(searchKeyword);
        return doctors.stream().map(doctor -> DoctorDTO.fromDoctor(doctor));
    }

    // TODO:
    @PostMapping("/{doctor_id}/weekly-schedules")
    public ResponseEntity<Object> createDoctorWeeklySchedule(@RequestBody WeeklySchedule weeklySchedule, @PathVariable Long doctor_id) {
        Doctor doctor = doctorRepository.findById(doctor_id).orElseThrow(
                () -> new ResourceNotFoundException("Doctor", doctor_id.toString()));
        weeklySchedule.setDoctor(doctor);
        weeklySchedule.setId(null);

        if(!isWeeklyScheduleCorrect(weeklySchedule))
            return ResponseEntity.badRequest().build();

        weeklyScheduleRepository.save(weeklySchedule);

        return ResponseEntity.noContent().build();
    }

    private boolean isWeeklyScheduleCorrect(WeeklySchedule weeklySchedule){
        List<WeeklySchedule> weeklySchedules = getWeeklySchedules(weeklySchedule.getDoctor().getId());
        OffsetDateTime startOfValidity =  weeklySchedule.startOfValidity;
        OffsetDateTime endOfValidity =  weeklySchedule.endOfValidity;
        if(startOfValidity.isAfter(endOfValidity))
            return false;
        for(WeeklySchedule ws : weeklySchedules){
            if(!(endOfValidity.isBefore(ws.startOfValidity) || startOfValidity.isAfter(ws.endOfValidity)))
                return false;
        }

        return true;
    }

}
