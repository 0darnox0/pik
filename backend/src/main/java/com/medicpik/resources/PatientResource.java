package com.medicpik.resources;

import com.medicpik.dto.PatientDTO;
import com.medicpik.dto.PatientRegistrationDTO;
import com.medicpik.entities.Patient;
import com.medicpik.repositories.PatientRepository;
import com.medicpik.repositories.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("/patients")
public class PatientResource {

    private final PatientRepository patientRepository;
    private final UserRepository userRepository;

    @Autowired
    public PatientResource(PatientRepository patientRepository, UserRepository userRepository) {
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
    }

    @GetMapping(value = "/{patient_id}", produces = "application/json")
    public PatientDTO getPatient(@PathVariable Long patient_id) {
        Patient patient = patientRepository.findById(patient_id).orElseThrow(
                () -> new ResourceNotFoundException("Patient", patient_id.toString()));

        return PatientDTO.fromPatient(patient);
    }

    @GetMapping(value = "", produces = "application/json")
    public Stream<PatientDTO> getPatients() {
        List<Patient> patients = patientRepository.findAll();
        return patients.stream().map(post -> PatientDTO.fromPatient(post));
    }

    @PostMapping("")
    public ResponseEntity<Object> registerPatient(@Valid @RequestBody PatientRegistrationDTO patientDTO) {
        patientDTO.setId(null);

        boolean duplicatedLogin = userRepository.findByLogin(patientDTO.getLogin()).isPresent();
        if (duplicatedLogin)
            return ResponseEntity.badRequest().build();

        boolean duplicatedPesel = patientRepository.findByPesel(patientDTO.getPesel()).isPresent();
        if (duplicatedPesel)
            return ResponseEntity.badRequest().build();

        Patient patient = PatientRegistrationDTO.toPatient(patientDTO);
        Patient savedPatient = patientRepository.save(patient);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedPatient.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
}
