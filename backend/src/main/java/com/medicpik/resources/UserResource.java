package com.medicpik.resources;

import com.medicpik.dto.UserDTO;
import com.medicpik.entities.Appointment;
import com.medicpik.entities.Doctor;
import com.medicpik.entities.Patient;
import com.medicpik.entities.User;
import com.medicpik.repositories.AppointmentRepository;
import com.medicpik.repositories.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserResource {

    @Autowired
    private final AppointmentRepository appointmentRepository;

    @Autowired
    private final UserRepository userRepository;

    public UserResource(AppointmentRepository appointmentRepository, UserRepository userRepository) {
        this.appointmentRepository = appointmentRepository;
        this.userRepository = userRepository;
    }

    @GetMapping(value = "/{user_id}", produces = "application/json")
    public User getUser(@PathVariable Long user_id) {
        User user = userRepository.findById(user_id).orElseThrow(
                () -> new ResourceNotFoundException("User", user_id.toString()));

        return user;
    }

    @GetMapping(value = "", produces = "application/json")
    public List<User> getUsers(@RequestParam(value = "login", required = false) String login) {
        if (login != null)
            return userRepository.findAllByLogin(login);

        return userRepository.findAll();
    }


    @GetMapping(value = "/{user_id}/appointments", produces = "application/json")
    public List<Appointment> getAppointments(@PathVariable Long user_id) {
        boolean exists = userRepository.existsById(user_id);

        if(!exists) {
            throw new ResourceNotFoundException("User", user_id.toString());
        }

        List<Appointment> appointments = appointmentRepository.findByPatientId(user_id);
        if (appointments.isEmpty()) {
            appointments = appointmentRepository.findByDoctorId(user_id);
        }

        return appointments;
    }

    @GetMapping(value = "/{user_id}/appointments/{appointment_id}", produces = "application/json")
    public Appointment getAppointment(@PathVariable Long user_id, @PathVariable Long appointment_id) {
        boolean exists = userRepository.existsById(user_id);

        if(!exists) {
            throw new ResourceNotFoundException("User", user_id.toString());
        }

        Optional<Appointment> appointment = appointmentRepository.findByIdAndPatientId(appointment_id, user_id);
        if (!appointment.isPresent()) {
            appointment = appointmentRepository.findByIdAndDoctorId(appointment_id, user_id);
        }
        if (!appointment.isPresent()) {
            throw new ResourceNotFoundException("Appointment", appointment_id.toString());
        }

        return appointment.get();
    }

    @PostMapping("")
    public ResponseEntity<Object> createUser(@RequestBody UserDTO userDTO) {
        userDTO.setId(null);

        if(userDTO.getRole().equals("patient")) {
            Patient user = UserDTO.toPatient(userDTO);
            if(!isNewPatientCorrect(user))
                return ResponseEntity.badRequest().build();
            userRepository.save(user);
        }
        else if(userDTO.getRole().equals("doctor")) {
            Doctor user = UserDTO.toDoctor(userDTO);
            if(!isNewDoctorCorrect(user))
                return ResponseEntity.badRequest().build();
            userRepository.save(user);
        }
        else return ResponseEntity.badRequest().build();

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{user_id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserDTO userDTO, @PathVariable Long user_id) {
        User user = userRepository.findById(user_id).orElseThrow(
                () -> new ResourceNotFoundException("User", user_id.toString()));

        user.setFirst_name(userDTO.getFirst_name());
        user.setLast_name(userDTO.getLast_name());
        if (userDTO.getPassword() != null && !userDTO.getPassword().isEmpty())
            user.setPassword(userDTO.getPassword());

        userRepository.save(user);

        return ResponseEntity.ok().build();
    }

    private boolean isNewDoctorCorrect(User user){

        return true;
    }

    private boolean isNewPatientCorrect(User user){

        return true;
    }
}
