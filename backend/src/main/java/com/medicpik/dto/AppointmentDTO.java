package com.medicpik.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicpik.entities.Appointment;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.time.OffsetDateTime;

public class AppointmentDTO {

    private Long id;
    private PatientDTO patient;
    private DoctorDTO doctor;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime date_start;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime date_end;
    private String room;
    private boolean canceled;
    private static ModelMapper modelMapper = new ModelMapper();

    public AppointmentDTO() {
    }

    public AppointmentDTO(Appointment appointment) {
        this.id = appointment.getId();
        this.patient = new PatientDTO(appointment.getPatient());
        this.doctor = new DoctorDTO(appointment.getDoctor());
        this.date_start = appointment.getDate_start();
        this.date_end = appointment.getDate_end();
        this.room = appointment.getRoom();
        this.canceled = appointment.isCanceled();
    }

    public static AppointmentDTO fromAppointment(Appointment appointment) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        AppointmentDTO dto = modelMapper.map(appointment, AppointmentDTO.class);
        return dto;
    }

    public static Appointment toAppointment(AppointmentDTO appointmentDTO) {
        return modelMapper.map(appointmentDTO, Appointment.class);
    }

    public Long getId() {
        return id;
    }

    public PatientDTO getPatient() {
        return patient;
    }

    public DoctorDTO getDoctor() {
        return doctor;
    }

    public String getRoom() {
        return room;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPatient(PatientDTO patient) {
        this.patient = patient;
    }

    public void setDoctor(DoctorDTO doctor) {
        this.doctor = doctor;
    }

    public void setDate_start(OffsetDateTime date_start) {
        this.date_start = date_start;
    }

    public void setDate_end(OffsetDateTime date_end) {
        this.date_end = date_end;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public OffsetDateTime getDate_start() { return date_start; }

    public OffsetDateTime getDate_end() { return date_end; }
}
