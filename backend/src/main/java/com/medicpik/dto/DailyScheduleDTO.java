package com.medicpik.dto;

import com.medicpik.entities.DailySchedule;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class DailyScheduleDTO {

    int week_day;
    HourDTO startHour;
    HourDTO endHour;
    static ModelMapper modelMapper = new ModelMapper();

    public DailyScheduleDTO() {
    }

    public DailyScheduleDTO(DailySchedule dailySchedule) {
        this.week_day = dailySchedule.getWeek_day();
        this.startHour = new HourDTO(dailySchedule.getStart_hour(), dailySchedule.getStart_minutes());
        this.endHour = new HourDTO(dailySchedule.getEnd_hour(), dailySchedule.getEnd_minutes());
    }

    public static DailyScheduleDTO fromDailySchedule(DailySchedule dailySchedule) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        DailyScheduleDTO dto = modelMapper.map(dailySchedule, DailyScheduleDTO.class);
        return dto;
    }

    public static DailySchedule toDailySchedule(DailyScheduleDTO dailyScheduleDTO) {
        return modelMapper.map(dailyScheduleDTO, DailySchedule.class);
    }

    public int getWeek_day() {
        return week_day;
    }

    public HourDTO getStartHour() {
        return startHour;
    }

    public HourDTO getEndHour() {
        return endHour;
    }

    public void setWeek_day(int week_day) {
        this.week_day = week_day;
    }

    public void setStartHour(HourDTO startHour) {
        this.startHour = startHour;
    }

    public void setEndHour(HourDTO endHour) {
        this.endHour = endHour;
    }
}
