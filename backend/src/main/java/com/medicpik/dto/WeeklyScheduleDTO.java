package com.medicpik.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicpik.entities.DailySchedule;
import com.medicpik.entities.WeeklySchedule;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;


public class WeeklyScheduleDTO {

    private Long id;
    private DoctorDTO doctor;
    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime startOfValidity;
    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    private OffsetDateTime endOfValidity;
    private List<DailyScheduleDTO> dailySchedules = new ArrayList<>();
    private static ModelMapper modelMapper = new ModelMapper();

    public WeeklyScheduleDTO() {
    }

    public WeeklyScheduleDTO(WeeklySchedule weeklySchedule) {
        this.id = weeklySchedule.getId();
        this.doctor = new DoctorDTO(weeklySchedule.getDoctor());
        this.startOfValidity  = weeklySchedule.getStartOfValidity();
        this.endOfValidity  = weeklySchedule.getEndOfValidity();
        List<DailySchedule> dschedules = weeklySchedule.getSchedule();
        for(DailySchedule ds : dschedules){
            this.dailySchedules.add(new DailyScheduleDTO(ds));
        }
    }

    public static WeeklyScheduleDTO fromWeeklySchedule(WeeklySchedule weeklySchedule) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        WeeklyScheduleDTO dto = modelMapper.map(weeklySchedule, WeeklyScheduleDTO.class);
//        Type listType = new TypeToken<List<DailyScheduleDTO>>() {}.getType();
//        dto.dailySchedules = modelMapper.map(weeklySchedule.daily_schedule, listType);
        return dto;
    }

    public static WeeklySchedule toWeeklySchedule(WeeklyScheduleDTO weeklyScheduleDTO) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
        return modelMapper.map(weeklyScheduleDTO, WeeklySchedule.class);
    }

    public Long getId() {
        return id;
    }

    public DoctorDTO getDoctor() {
        return doctor;
    }

    public OffsetDateTime getStartOfValidity() {
        return startOfValidity;
    }

    public OffsetDateTime getEndOfValidity() {
        return endOfValidity;
    }

    public List<DailyScheduleDTO> getDailySchedules() {
        return dailySchedules;
    }

    public static ModelMapper getModelMapper() {
        return modelMapper;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDoctor(DoctorDTO doctor) {
        this.doctor = doctor;
    }

    public void setStartOfValidity(OffsetDateTime startOfValidity) {
        this.startOfValidity = startOfValidity;
    }

    public void setEndOfValidity(OffsetDateTime endOfValidity) {
        this.endOfValidity = endOfValidity;
    }

    public void setDailySchedules(List<DailyScheduleDTO> dailySchedules) {
        this.dailySchedules = dailySchedules;
    }

    public static void setModelMapper(ModelMapper modelMapper) {
        WeeklyScheduleDTO.modelMapper = modelMapper;
    }
}
