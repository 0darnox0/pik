package com.medicpik.dto;

import com.medicpik.entities.Patient;
import org.hibernate.validator.constraints.Length;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import javax.validation.constraints.NotNull;

public class PatientRegistrationDTO {

    Long id;

    @NotNull
    @Length(min=2)
    String first_name;

    @NotNull
    @Length(min=2)
    String last_name;

    @NotNull
    @Length(min=4)
    String login;

    @NotNull
    @Length(min=5)
    String password;

    @NotNull
    @Length(min=11, max=11)
    String pesel;

    private static ModelMapper modelMapper = new ModelMapper();

    public PatientRegistrationDTO() {
    }

    public static PatientRegistrationDTO fromPatient(Patient patient) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        PatientRegistrationDTO dto = modelMapper.map(patient, PatientRegistrationDTO.class);
        return dto;
    }

    public static Patient toPatient(PatientRegistrationDTO patientDTO) {
        return modelMapper.map(patientDTO, Patient.class);
    }

    public Long getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getPesel() {
        return pesel;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
