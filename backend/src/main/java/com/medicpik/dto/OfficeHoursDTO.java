package com.medicpik.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.OffsetDateTime;

public class OfficeHoursDTO implements Serializable {
    public DoctorDTO doctor;

    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    public OffsetDateTime start;
    @JsonFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssZ")
    public OffsetDateTime end;
    public boolean occupied = false;
    public Long appointment_id;

    public OfficeHoursDTO(DoctorDTO doctor, OffsetDateTime start, OffsetDateTime end) {
        this.doctor = doctor;
        this.start = start;
        this.end = end;
    }

    public OfficeHoursDTO(DoctorDTO doctor, OffsetDateTime start, OffsetDateTime end, boolean occupied, Long appointment_id) {
        this.doctor = doctor;
        this.start = start;
        this.end = end;
        this.occupied = occupied;
        this.appointment_id = appointment_id;
    }
}
