package com.medicpik.dto;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

// unused
public class HourDTO {

    private int hour;
    private int minutes;

    public HourDTO() {
    }

    public HourDTO(int hour, int minutes) {
        if(hour < 0 || hour > 23 || minutes < 0 || minutes >59)
            throw new Error("Incorrect hour values. Impossible to create object!");
        this.hour = hour;
        this.minutes = minutes;
    }


    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHour() {
        return hour;
    }

    public int getMinutes() {
        return minutes;
    }

}
