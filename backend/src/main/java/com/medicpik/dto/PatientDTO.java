package com.medicpik.dto;

import com.medicpik.entities.Patient;
import com.medicpik.entities.WeeklySchedule;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class PatientDTO {

    Long id;
    String first_name;
    String last_name;
    static ModelMapper modelMapper = new ModelMapper();

    public PatientDTO(){}

    public PatientDTO(Patient patient){
        this.id = patient.getId();
        this.first_name = patient.getFirst_name();
        this.last_name = patient.getLast_name();
    }

    public static PatientDTO fromPatient(Patient patient) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        PatientDTO dto = modelMapper.map(patient, PatientDTO.class);
        return dto;
    }

    public static Patient toPatient(PatientDTO patientDTO) {
        return modelMapper.map(patientDTO, Patient.class);
    }

    public Long getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }


}
