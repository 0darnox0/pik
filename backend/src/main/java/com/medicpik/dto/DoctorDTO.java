package com.medicpik.dto;

import com.medicpik.entities.Doctor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class DoctorDTO {

    private Long id;
    private String first_name;
    private String last_name;
    private String specialization;
    static ModelMapper modelMapper = new ModelMapper();

    public DoctorDTO() {
    }

    public DoctorDTO(Doctor doctor) {
        this.id = doctor.getId();
        this.first_name = doctor.getFirst_name();
        this.last_name = doctor.getLast_name();
        this.specialization = doctor.getSpecialization();
    }

    public static DoctorDTO fromDoctor(Doctor doctor) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        DoctorDTO dto = modelMapper.map(doctor, DoctorDTO.class);
        return dto;
    }

    public static Doctor toDoctor(DoctorDTO doctorDTO) {
        return modelMapper.map(doctorDTO, Doctor.class);
    }

    public Long getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
