package com.medicpik.dto;

import com.medicpik.entities.User;
import org.modelmapper.convention.MatchingStrategies;

public class LoginResponseDTO extends UserDTO {
    private String token;

    public LoginResponseDTO() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static LoginResponseDTO fromUser(User user) {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        return modelMapper.map(user, LoginResponseDTO.class);
    }
}
