package com.medicpik.dto;

import com.medicpik.entities.Doctor;
import com.medicpik.entities.Patient;
import com.medicpik.entities.User;
import org.modelmapper.ModelMapper;

public class UserDTO {

    Long id;
    String role;
    String login;
    String first_name;
    String last_name;
    String password; // do pobierania
    String pesel; // null if doctor
    String specialization; // null if patient
    protected static ModelMapper modelMapper = new ModelMapper();

    public UserDTO() {
    }

    public UserDTO(Long id, String login, String first_name, String last_name, String password, String pesel, String specialization) {
        this.id = id;
        this.login = login;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.pesel = pesel;
        this.specialization = specialization;
    }

    public static UserDTO fromPatient(Patient patient) {
        UserDTO dto = modelMapper.map(patient, UserDTO.class);
        return dto;
    }

    public static UserDTO fromDoctor(Patient patient) {
        UserDTO dto = modelMapper.map(patient, UserDTO.class);
        return dto;
    }

    public static Patient toPatient(UserDTO userDTO) {
        return modelMapper.map(userDTO, Patient.class);
    }

    public static Doctor toDoctor(UserDTO userDTO) {
        return modelMapper.map(userDTO, Doctor.class);
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPassword() {
        return password;
    }

    public String getPesel() {
        return pesel;
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getRole() {
        return role;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }
}
