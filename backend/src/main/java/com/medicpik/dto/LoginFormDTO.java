package com.medicpik.dto;

import javax.validation.constraints.NotNull;

public class LoginFormDTO {

    @NotNull
    private String login;
    @NotNull
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
