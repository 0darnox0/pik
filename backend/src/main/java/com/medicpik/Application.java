package com.medicpik;

import com.medicpik.entities.*;
import com.medicpik.repositories.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@SpringBootApplication
@EnableJpaRepositories
public class Application implements CommandLineRunner {

    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final WeeklyScheduleRepository weeklyScheduleRepository;
    private final AppointmentRepository appointmentRepository;
    private final LeaveRepository leaveRepository;

    @Autowired
    public Application(PatientRepository patientRepository, DoctorRepository doctorRepository, WeeklyScheduleRepository weeklyScheduleRepository, AppointmentRepository appointmentRepository, LeaveRepository leaveRepository) {
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
        this.weeklyScheduleRepository = weeklyScheduleRepository;
        this.appointmentRepository = appointmentRepository;
        this.leaveRepository = leaveRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
//        populateTestData();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

//    private void populateTestData() {
//        Patient patient1 = new Patient("Jan", "Kowalski", "user", "user","23546545980");
//        Patient patient2 = new Patient("Olgierd", "Iks", "qaz", "qaz","23546545347");
//        Doctor doctor1 = new Doctor("John", "Smith", "doctor", "doctor", "dentist");
//        Doctor doctor2 = new Doctor("Henryk", "Walc", "qwe", "qwe", "oculist");
//
//        WeeklySchedule ws = new WeeklySchedule(doctor1, LocalDateTime.of(2019,05,1,00,00).atOffset(ZoneOffset.of("+02:00")),  LocalDateTime.of(2019,06,30,00,00).atOffset(ZoneOffset.of("+02:00")) );
//        ws.addDailySchedule(new DailySchedule(1, 8, 0, 16, 0, ws));
//        ws.addDailySchedule(new DailySchedule(2, 9, 0, 16, 0, ws));
//        ws.addDailySchedule(new DailySchedule(3, 8, 0, 12, 0, ws));
//        ws.addDailySchedule(new DailySchedule(4,8, 0, 16, 0, ws));
//        ws.addDailySchedule(new DailySchedule(5, 8, 0, 13, 0, ws));
//
//        Appointment appointment = new Appointment(patient1, doctor1, LocalDateTime.of(2019, 06,24,11, 00).atOffset(ZoneOffset.of("+02:00")), LocalDateTime.of(2019,06,24,12,00).atOffset(ZoneOffset.of("+02:00")), "402A", false );
//        Appointment appointment2 = new Appointment(patient1, doctor1, LocalDateTime.of(2019, 06,11,10, 00).atOffset(ZoneOffset.of("+02:00")), LocalDateTime.of(2019,06,11,11,00).atOffset(ZoneOffset.of("+02:00")), "22", false );
//        Leave leave = new Leave(doctor1, LocalDateTime.of(2019, 06,24,15, 30).atOffset(ZoneOffset.of("+02:00")), LocalDateTime.of(2019,06,24,16,00).atOffset(ZoneOffset.of("+02:00")));
//
//        // save objects
//        patientRepository.save(patient1);
//        patientRepository.save(patient2);
//
//        doctorRepository.save(doctor1);
//        doctorRepository.save(doctor2);
//
//        weeklyScheduleRepository.save(ws);
//        appointmentRepository.save(appointment);
//        appointmentRepository.save(appointment2);
//        leaveRepository.save(leave);
//    }

}