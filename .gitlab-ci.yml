image: maven:3-jdk-8-alpine

variables:
  DOCKER_DRIVER: overlay2
  SPRING_PROFILES_ACTIVE: gitlab-ci

  ROLLOUT_RESOURCE_TYPE: deployment


stages:
  - test
  - build
  - package


test_frontend:
  stage: test
  image: node:latest
  script:
    - cd frontend/
    - apt-get update -yqqq
    - apt-get install -y xvfb
    - apt-get install iceweasel -yqq
    - Xvfb :99 -ac &
    - export DISPLAY=:99
    - npm install --quiet
    - npm run test-ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
  only:
    - branches
    - tags
  except:
    variables:
      - $TEST_DISABLED
  artifacts:
    paths:
      - frontend/coverage/
    reports:
      junit:
        - frontend/unittest_report.xml


test_backend:
  stage: test
  image: maven:3-jdk-8-alpine
  script:
    - cd backend/
    - mvn test
    - cat target/site/jacoco/index.html | grep -o 'Total[^%]*%' | sed -e 's/Total/Jacoco-Test-Coverage:/g'
  coverage: '/Jacoco-Test-Coverage:.+(\d+\.?\d+)%/'
  only:
    - branches
    - tags
  except:
    variables:
      - $TEST_DISABLED
  artifacts:
    reports:
      junit:
        - backend/target/surefire-reports/TEST-*.xml
    paths:
      - backend/target/site/jacoco/


code_quality:
  stage: test
  image: docker:stable
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - setup_docker
    - code_quality
  artifacts:
    paths: [gl-code-quality-report.json]
  only:
    - branches
    - tags
  except:
    variables:
      - $CODE_QUALITY_DISABLED


maven-build:
  stage: build
  image: maven:3-jdk-8-alpine
  script:
    - cd backend/
    - mvn package -B
  artifacts:
    paths:
      - backend/target/*.jar


front-build:
  stage: build
  image: node:latest
  script:
    - cd frontend/
    - apt-get update -yqqq
    - apt-get install -y xvfb
    - apt-get install iceweasel -yqq
    - Xvfb :99 -ac &
    - export DISPLAY=:99
    - npm install --quiet
    - npm run build --prod
    - tar czf front-dist.tar.gz dist
  only:
    - branches
    - tags
  artifacts:
    paths:
      - frontend/front-dist.tar.gz


docker-build:
  stage: package
  image: docker:stable
  services:
    - docker:stable-dind
  script:
    - cd backend/
    - docker build -t registry.gitlab.com/0darnox0/pik .
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker push registry.gitlab.com/0darnox0/pik
  only:
    - branches
    - tags


pages:
  stage: package
  dependencies:
    - test_frontend
    - test_backend
  script:
    - mkdir public/
    - mv frontend/coverage/WebPanel/ public/frontend/
    - mv backend/target/site/jacoco/ public/backend/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master


# ---------------------------------------------------------------------------

.auto_devops: &auto_devops |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x
  auto_database_url=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${CI_ENVIRONMENT_SLUG}-postgres:5432/${POSTGRES_DB}
  export DATABASE_URL=${DATABASE_URL-$auto_database_url}
  if [[ -z "$CI_COMMIT_TAG" ]]; then
    export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    export CI_APPLICATION_TAG=$CI_COMMIT_SHA
  else
    export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE
    export CI_APPLICATION_TAG=$CI_COMMIT_TAG
  fi
  export TILLER_NAMESPACE=$KUBE_NAMESPACE
  # Extract "MAJOR.MINOR" from CI_SERVER_VERSION and generate "MAJOR-MINOR-stable" for Security Products
  export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')

  function registry_login() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
      echo ""
    fi
  }

  function container_scanning() {
    registry_login

    docker run -d --name db arminc/clair-db:latest
    docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.6
    apk add -U wget ca-certificates
    docker pull ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
    wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    mv clair-scanner_linux_amd64 clair-scanner
    chmod +x clair-scanner
    touch clair-whitelist.yml
    retries=0
    echo "Waiting for clair daemon to start"
    while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} || true
  }

  function code_quality() {
    docker run --env SOURCE_CODE="$PWD" \
               --volume "$PWD":/code \
               --volume /var/run/docker.sock:/var/run/docker.sock \
               "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  }

  function license_management() {
    /run.sh analyze .
  }

  function sast() {
    case "$CI_SERVER_VERSION" in
      *-ee)

        # Deprecation notice for CONFIDENCE_LEVEL variable
        if [ -z "$SAST_CONFIDENCE_LEVEL" -a "$CONFIDENCE_LEVEL" ]; then
          SAST_CONFIDENCE_LEVEL="$CONFIDENCE_LEVEL"
          echo "WARNING: CONFIDENCE_LEVEL is deprecated and MUST be replaced with SAST_CONFIDENCE_LEVEL"
        fi

        docker run --env SAST_CONFIDENCE_LEVEL="${SAST_CONFIDENCE_LEVEL:-3}" \
                   --volume "$PWD:/code" \
                   --volume /var/run/docker.sock:/var/run/docker.sock \
                   "registry.gitlab.com/gitlab-org/security-products/sast:$SP_VERSION" /app/bin/run /code
        ;;
      *)
        echo "GitLab EE is required"
        ;;
    esac
  }

  function dependency_scanning() {
    case "$CI_SERVER_VERSION" in
      *-ee)
        docker run --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}" \
                   --volume "$PWD:/code" \
                   --volume /var/run/docker.sock:/var/run/docker.sock \
                   "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code
        ;;
      *)
        echo "GitLab EE is required"
        ;;
    esac
  }

  function get_replicas() {
    track="${1:-stable}"
    percentage="${2:-100}"

    env_track=$( echo $track | tr -s  '[:lower:]'  '[:upper:]' )
    env_slug=$( echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s  '[:lower:]'  '[:upper:]' )

    if [[ "$track" == "stable" ]] || [[ "$track" == "rollout" ]]; then
      # for stable track get number of replicas from `PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        new_replicas=$REPLICAS
      fi
    else
      # for all tracks get number of replicas from `CANARY_PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_track}_${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        eval new_replicas=\${env_track}_REPLICAS
      fi
    fi

    replicas="${new_replicas:-1}"
    replicas="$(($replicas * $percentage / 100))"

    # always return at least one replicas
    if [[ $replicas -gt 0 ]]; then
      echo "$replicas"
    else
      echo 1
    fi
  }

  # Extracts variables prefixed with K8S_SECRET_
  # and creates a Kubernetes secret.
  #
  # e.g. If we have the following environment variables:
  #   K8S_SECRET_A=value1
  #   K8S_SECRET_B=multi\ word\ value
  #
  # Then we will create a secret with the following key-value pairs:
  #   data:
  #     A: dmFsdWUxCg==
  #     B: bXVsdGkgd29yZCB2YWx1ZQo=
  function create_application_secret() {
    track="${1-stable}"
    export APPLICATION_SECRET_NAME=$(application_secret_name "$track")

    env | sed -n "s/^K8S_SECRET_\(.*\)$/\1/p" > k8s_prefixed_variables

    kubectl create secret \
      -n "$KUBE_NAMESPACE" generic "$APPLICATION_SECRET_NAME" \
      --from-env-file k8s_prefixed_variables -o yaml --dry-run |
      kubectl replace -n "$KUBE_NAMESPACE" --force -f -

    export APPLICATION_SECRET_CHECKSUM=$(cat k8s_prefixed_variables | sha256sum | cut -d ' ' -f 1)

    rm k8s_prefixed_variables
  }

  function deploy_name() {
    name="$CI_ENVIRONMENT_SLUG"
    track="${1-stable}"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    echo $name
  }

  function application_secret_name() {
    track="${1-stable}"
    name=$(deploy_name "$track")

    echo "${name}-secret"
  }

  function deploy() {
    track="${1-stable}"
    percentage="${2:-100}"
    name=$(deploy_name "$track")

    replicas="1"
    service_enabled="true"
    postgres_enabled="$POSTGRES_ENABLED"

    # if track is different than stable,
    # re-use all attached resources
    if [[ "$track" != "stable" ]]; then
      service_enabled="false"
      postgres_enabled="false"
    fi

    replicas=$(get_replicas "$track" "$percentage")

    if [[ "$CI_PROJECT_VISIBILITY" != "public" ]]; then
      secret_name='gitlab-registry'
    else
      secret_name=''
    fi

    create_application_secret "$track"

    env_slug=$(echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s '[:lower:]' '[:upper:]')
    eval env_ADDITIONAL_HOSTS=\$${env_slug}_ADDITIONAL_HOSTS
    if [ -n "$env_ADDITIONAL_HOSTS" ]; then
      additional_hosts="{$env_ADDITIONAL_HOSTS}"
    elif [ -n "$ADDITIONAL_HOSTS" ]; then
      additional_hosts="{$ADDITIONAL_HOSTS}"
    fi

    if [[ -n "$DB_INITIALIZE" && -z "$(helm ls -q "^$name$")" ]]; then
      echo "Deploying first release with database initialization..."
      helm upgrade --install \
        --wait \
        --set service.enabled="$service_enabled" \
        --set gitlab.app="$CI_PROJECT_PATH_SLUG" \
        --set gitlab.env="$CI_ENVIRONMENT_SLUG" \
        --set releaseOverride="$CI_ENVIRONMENT_SLUG" \
        --set image.repository="$CI_APPLICATION_REPOSITORY" \
        --set image.tag="$CI_APPLICATION_TAG" \
        --set image.pullPolicy=IfNotPresent \
        --set image.secrets[0].name="$secret_name" \
        --set application.track="$track" \
        --set application.database_url="$DATABASE_URL" \
        --set application.secretName="$APPLICATION_SECRET_NAME" \
        --set application.secretChecksum="$APPLICATION_SECRET_CHECKSUM" \
        --set service.commonName="le.$KUBE_INGRESS_BASE_DOMAIN" \
        --set service.url="$CI_ENVIRONMENT_URL" \
        --set service.additionalHosts="$additional_hosts" \
        --set replicaCount="$replicas" \
        --set postgresql.enabled="$postgres_enabled" \
        --set postgresql.nameOverride="postgres" \
        --set postgresql.postgresUser="$POSTGRES_USER" \
        --set postgresql.postgresPassword="$POSTGRES_PASSWORD" \
        --set postgresql.postgresDatabase="$POSTGRES_DB" \
        --set postgresql.imageTag="$POSTGRES_VERSION" \
        --set application.initializeCommand="$DB_INITIALIZE" \
        --namespace="$KUBE_NAMESPACE" \
        "$name" \
        chart/

      echo "Deploying second release..."
      helm upgrade --reuse-values \
        --wait \
        --set application.initializeCommand="" \
        --set application.migrateCommand="$DB_MIGRATE" \
        --namespace="$KUBE_NAMESPACE" \
        "$name" \
        chart/
    else
      echo "Deploying new release..."
      helm upgrade --install \
        --wait \
        --set service.enabled="$service_enabled" \
        --set gitlab.app="$CI_PROJECT_PATH_SLUG" \
        --set gitlab.env="$CI_ENVIRONMENT_SLUG" \
        --set releaseOverride="$CI_ENVIRONMENT_SLUG" \
        --set image.repository="$CI_APPLICATION_REPOSITORY" \
        --set image.tag="$CI_APPLICATION_TAG" \
        --set image.pullPolicy=IfNotPresent \
        --set image.secrets[0].name="$secret_name" \
        --set application.track="$track" \
        --set application.database_url="$DATABASE_URL" \
        --set application.secretName="$APPLICATION_SECRET_NAME" \
        --set application.secretChecksum="$APPLICATION_SECRET_CHECKSUM" \
        --set service.commonName="le.$KUBE_INGRESS_BASE_DOMAIN" \
        --set service.url="$CI_ENVIRONMENT_URL" \
        --set service.additionalHosts="$additional_hosts" \
        --set replicaCount="$replicas" \
        --set postgresql.enabled="$postgres_enabled" \
        --set postgresql.nameOverride="postgres" \
        --set postgresql.postgresUser="$POSTGRES_USER" \
        --set postgresql.postgresPassword="$POSTGRES_PASSWORD" \
        --set postgresql.postgresDatabase="$POSTGRES_DB" \
        --set application.migrateCommand="$DB_MIGRATE" \
        --namespace="$KUBE_NAMESPACE" \
        "$name" \
        chart/
    fi

    kubectl rollout status -n "$KUBE_NAMESPACE" -w "$ROLLOUT_RESOURCE_TYPE/$name"
  }

  function scale() {
    track="${1-stable}"
    percentage="${2-100}"
    name=$(deploy_name "$track")

    replicas=$(get_replicas "$track" "$percentage")

    if [[ -n "$(helm ls -q "^$name$")" ]]; then
      helm upgrade --reuse-values \
        --wait \
        --set replicaCount="$replicas" \
        --namespace="$KUBE_NAMESPACE" \
        "$name" \
        chart/
    fi
  }

  function install_dependencies() {
    apk add -U openssl curl tar gzip bash ca-certificates git
    curl -sSL -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    curl -sSL -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
    apk add glibc-2.28-r0.apk
    rm glibc-2.28-r0.apk

    curl -sS "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
    mv linux-amd64/helm /usr/bin/
    mv linux-amd64/tiller /usr/bin/
    helm version --client
    tiller -version

    curl -sSL -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client
  }

  function setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

  function setup_test_db() {
    if [ -z ${KUBERNETES_PORT+x} ]; then
      DB_HOST=postgres
    else
      DB_HOST=localhost
    fi
    export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${DB_HOST}:5432/${POSTGRES_DB}"
  }

  function download_chart() {
    if [[ ! -d chart ]]; then
      auto_chart=${AUTO_DEVOPS_CHART:-gitlab/auto-deploy-app}
      auto_chart_name=$(basename $auto_chart)
      auto_chart_name=${auto_chart_name%.tgz}
      auto_chart_name=${auto_chart_name%.tar.gz}
    else
      auto_chart="chart"
      auto_chart_name="chart"
    fi

    helm init --client-only
    helm repo add gitlab ${AUTO_DEVOPS_CHART_REPOSITORY:-https://charts.gitlab.io}
    if [[ ! -d "$auto_chart" ]]; then
      helm fetch ${auto_chart} --untar
    fi
    if [ "$auto_chart_name" != "chart" ]; then
      mv ${auto_chart_name} chart
    fi

    helm dependency update chart/
    helm dependency build chart/
  }

  function ensure_namespace() {
    kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }


  # Function to ensure backwards compatibility with AUTO_DEVOPS_DOMAIN
  function ensure_kube_ingress_base_domain() {
    if [ -z ${KUBE_INGRESS_BASE_DOMAIN+x} ] && [ -n "$AUTO_DEVOPS_DOMAIN" ] ; then
      export KUBE_INGRESS_BASE_DOMAIN=$AUTO_DEVOPS_DOMAIN
    fi
  }

  function check_kube_domain() {
    ensure_kube_ingress_base_domain

    if [ -z ${KUBE_INGRESS_BASE_DOMAIN+x} ]; then
      echo "In order to deploy or use Review Apps,"
      echo "AUTO_DEVOPS_DOMAIN or KUBE_INGRESS_BASE_DOMAIN variables must be set"
      echo "From 11.8, you can set KUBE_INGRESS_BASE_DOMAIN in cluster settings"
      echo "or by defining a variable at group or project level."
      echo "You can also manually add it in .gitlab-ci.yml"
      echo "AUTO_DEVOPS_DOMAIN support will be dropped on 12.0"
      false
    else
      true
    fi
  }

  function initialize_tiller() {
    echo "Checking Tiller..."

    export HELM_HOST="localhost:44134"
    tiller -listen ${HELM_HOST} -alsologtostderr > /dev/null 2>&1 &
    echo "Tiller is listening on ${HELM_HOST}"

    if ! helm version --debug; then
      echo "Failed to init Tiller."
      return 1
    fi
    echo ""
  }

  function create_secret() {
    echo "Create secret..."
    if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
      return
    fi

    kubectl create secret -n "$KUBE_NAMESPACE" \
      docker-registry gitlab-registry \
      --docker-server="$CI_REGISTRY" \
      --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
      --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
      --docker-email="$GITLAB_USER_EMAIL" \
      -o yaml --dry-run | kubectl replace -n "$KUBE_NAMESPACE" --force -f -
  }

  function dast() {
    export CI_ENVIRONMENT_URL=$(cat environment_url.txt)

    mkdir /zap/wrk/
    /zap/zap-baseline.py -J gl-dast-report.json -t "$CI_ENVIRONMENT_URL" || true
    cp /zap/wrk/gl-dast-report.json .
  }

  function performance() {
    export CI_ENVIRONMENT_URL=$(cat environment_url.txt)

    mkdir gitlab-exporter
    wget -O gitlab-exporter/index.js https://gitlab.com/gitlab-org/gl-performance/raw/10-5/index.js

    mkdir sitespeed-results

    if [ -f .gitlab-urls.txt ]
    then
      sed -i -e 's@^@'"$CI_ENVIRONMENT_URL"'@' .gitlab-urls.txt
      docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:6.3.1 --plugins.add ./gitlab-exporter --outputFolder sitespeed-results .gitlab-urls.txt
    else
      docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:6.3.1 --plugins.add ./gitlab-exporter --outputFolder sitespeed-results "$CI_ENVIRONMENT_URL"
    fi

    mv sitespeed-results/data/performance.json performance.json
  }

  function persist_environment_url() {
      echo $CI_ENVIRONMENT_URL > environment_url.txt
  }

  function delete() {
    track="${1-stable}"
    name=$(deploy_name "$track")

    if [[ -n "$(helm ls -q "^$name$")" ]]; then
      helm delete --purge "$name"
    fi

    secret_name=$(application_secret_name "$track")
    kubectl delete secret --ignore-not-found -n "$KUBE_NAMESPACE" "$secret_name"
  }

before_script:
  - *auto_devops
