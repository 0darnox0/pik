import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { map } from 'rxjs/operators';
import { User } from '../model/user';
import { BaseCrudService } from './base-crud.service';
import { Appointment } from '../model/appointment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseCrudService<User> {
  private static relativeUrl: string = '/users';

  constructor(restService: RestService) {
    super(restService, UserService.relativeUrl);
  }
 
  create(item: User) {
    return super.create(item);
  }

  get(id: number) {
    return super.get(id).pipe(map(data => new User(data)));
  }

  getAll() {
    return super.getAll().pipe(map(data => data.map(data => new User(data))));
  }

  getAllByLogin(login: string): Observable<User[]> {
    const url = `${this.relativePath}?login=${login}`;
    return this.restService.get<User[]>(url).pipe(map(data => data.map(data => new User(data))));
  }

  update(id: number, item: User) {
    return super.update(id, item);
  }

  delete(id: number) {
    return super.delete(id);
  }

  getAppointments(id: number): Observable<Appointment[]> {
    const url = `${this.getItemUrl(id)}/appointments`;
    return this.restService.get<Appointment[]>(url).pipe(map(data => data.map(data => new Appointment(data))));
  }
}
