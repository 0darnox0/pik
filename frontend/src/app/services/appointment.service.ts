import { Injectable } from '@angular/core';

import { Appointment } from '../model/appointment';
import { BaseCrudService } from './base-crud.service';
import { RestService } from './rest.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AppointmentService extends BaseCrudService<Appointment> {

  private static relativeUrl: string = '/appointments';

  constructor(restService: RestService) {
    super(restService, AppointmentService.relativeUrl);
  }

  create(item: Appointment) {
    return super.create(item);
  }

  get(id: number) {
    return super.get(id).pipe(map(data => new Appointment(data)));
  }

  getAll() {
    return super.getAll().pipe(map(data => data.map(data => new Appointment(data))));
  }

  update(id: number, item: Appointment) {
    return super.update(id, item);
  }

  delete(id: number) {
    return super.delete(id);
  }
  
  public getUserAppointments(user_id: number): Observable<Appointment[]> {
    const url = `${this.relativePath}?user=${user_id}`;
    return this.restService.get<Appointment[]>(url).pipe(map(data => data.map(data => new Appointment(data))));
  }
  
}
