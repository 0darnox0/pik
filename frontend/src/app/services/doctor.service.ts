import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Doctor } from '../model/doctor';
import { Appointment } from '../model/appointment';
import { OfficeHour } from '../model/office-hour';
import { BaseCrudService } from './base-crud.service';
import { RestService } from './rest.service';
import { map } from 'rxjs/operators';
import { WeeklySchedule } from '../model/weekly-schedule';


@Injectable({
  providedIn: 'root'
})
export class DoctorService extends BaseCrudService<Doctor> {

  private static relativeUrl: string = '/doctors';

  constructor(restService: RestService) {
    super(restService, DoctorService.relativeUrl);
  }

  create(item: Doctor) {
    return super.create(item);
  }

  get(id: number) {
    return super.get(id).pipe(map(data => new Doctor(data)));
  }

  getAll() {
    return super.getAll().pipe(map(data => data.map(data => new Doctor(data))));
  }

  update(id: number, item: Doctor) {
    return super.update(id, item);
  }

  delete(id: number) {
    return super.delete(id);
  }

  search(term: string): Observable<Doctor[]> {
    const url = `${this.relativePath}/search/${term}`
    return this.restService.get<Doctor[]>(url).pipe(map(data => data.map(data => new Doctor(data))));
  }

  getAppointment(doctorId: number, appointmentId: number): Observable<Appointment> {
    const url = `${this.getItemUrl(doctorId)}/appointments/${appointmentId}`
    return this.restService.get<Appointment>(url).pipe(map(data => new Appointment(data)));
  }

  getAppointments(id: number): Observable<Appointment[]> {
    const url = `${this.getItemUrl(id)}/appointments`
    return this.restService.get<Appointment[]>(url).pipe(map(data => data.map(data => new Appointment(data))));
  }

  getOfficeHours(id: number): Observable<OfficeHour[]> {
    const url = `${this.getItemUrl(id)}/office-hours`
    return this.restService.get<OfficeHour[]>(url).pipe(map(data => data.map(data => new OfficeHour(data))));
  }

  createWeeklySchedule(doctorId: number, item: WeeklySchedule) {
    const url = `${this.getItemUrl(doctorId)}/weekly-schedules`;
    return this.restService.post<WeeklySchedule>(url, item);
  }

  getWeeklySchedule(doctorId: number, scheduleId: number): Observable<WeeklySchedule>  {
    const url = `${this.getItemUrl(doctorId)}/weekly-schedules/${scheduleId}`;
    return this.restService.get<WeeklySchedule>(url).pipe(map(data => new WeeklySchedule(data)));
  }

  getWeeklySchedules(doctorId: number): Observable<WeeklySchedule[]>  {
    const url = `${this.getItemUrl(doctorId)}/weekly-schedules`;
    return this.restService.get<WeeklySchedule[]>(url).pipe(map(data => data.map(data => new WeeklySchedule(data))));
  }
}
