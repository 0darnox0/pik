import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  protected baseUrl: string = 'http://localhost:8080';
  
  protected httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'token': "" // TODO: add token
    }),
  };

  protected constructor(private httpClient: HttpClient){}

  get<T>(relativeUrl: string) {
    console.log("GET: " + this.getUrl(relativeUrl));
    return this.httpClient.get<T>(this.getUrl(relativeUrl), this.httpOptions);
  }
  
  post<T>(relativeUrl: string, item: any) {
    console.log("POST: " + this.getUrl(relativeUrl));
    console.log(item);
    return this.httpClient.post<T>(this.getUrl(relativeUrl), item, this.httpOptions);
  }

  put<T>(relativeUrl: string, item: any) {
    console.log("PUT: " + this.getUrl(relativeUrl));
    return this.httpClient.put<T>(this.getUrl(relativeUrl), item, this.httpOptions);
  }

  delete(relativeUrl: string) {
    console.log("DELETE: " + this.getUrl(relativeUrl));
    return this.httpClient.delete(this.getUrl(relativeUrl), this.httpOptions);
  }

  protected getUrl(relativeUrl: string): string {
    return new URL(relativeUrl, this.baseUrl).toString();
  }
  
}
