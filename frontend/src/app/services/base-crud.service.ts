import { RestService } from './rest.service';
import { Observable } from 'rxjs';


export abstract class BaseCrudService<T> {

  constructor(protected restService: RestService, protected relativePath: string) { }

  protected create(item: T) {
    return this.restService.post<T>(this.relativePath, item);
  }

  protected get(id: number): Observable<T> {
    return this.restService.get<T>(this.getItemUrl(id));
  }

  protected getAll(): Observable<T[]> {
    return this.restService.get<T[]>(this.relativePath);
  }

  protected update(id: number, item: T) {
    return this.restService.put<T>(this.getItemUrl(id), item);
  }

  protected delete(id: number) {
    return this.restService.delete(this.getItemUrl(id));
  }

  protected getItemUrl(id: number): string {
    return `${this.relativePath}/${id}`;
  }
}
