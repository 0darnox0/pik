import { TestBed } from '@angular/core/testing';

import { BaseCrudService } from './base-crud.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BaseCrudService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule ],
  }));
});
