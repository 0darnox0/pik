import { TestBed } from '@angular/core/testing';

import { DoctorService } from './doctor.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DoctorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule ],
  }));

  it('should be created', () => {
    const service: DoctorService = TestBed.get(DoctorService);
    expect(service).toBeTruthy();
  });
});
