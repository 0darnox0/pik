import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../model/user';
import { RestService } from './rest.service';
import { map, take, concatMap } from 'rxjs/operators';
import { Role } from '../model/role';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private restService: RestService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(login: string, password: string) {
        return this.restService.post<any>(`/auth/login`, { login, password })
            .pipe(map(user => {
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    isLoggedIn(): boolean {
        return this.currentUserValue != null;
    }

    isDoctor(): boolean {
        return this.currentUserValue && this.currentUserValue.role == Role.Doctor;
    }

    isPatient(): boolean {
        return this.currentUserValue && this.currentUserValue.role == Role.Patient;
    }

    get currentUserID(): number|null {
        if(this.isLoggedIn())
            return this.currentUserValue.id;
        
        return null;
    }
}
