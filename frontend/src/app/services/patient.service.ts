import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Patient } from '../model/patient';
import { Appointment } from '../model/appointment';
import { BaseCrudService } from './base-crud.service';
import { RestService } from './rest.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PatientService extends BaseCrudService<Patient> {

  private static relativeUrl: string = '/patients';

  constructor(restService: RestService) {
    super(restService, PatientService.relativeUrl);
  }

  create(item: Patient) {
    return super.create(item);
  }

  get(id: number) {
    return super.get(id).pipe(map(data => new Patient(data)));
  }

  getAll() {
    return super.getAll().pipe(map(data => data.map(data => new Patient(data))));
  }

  update(id: number, item: Patient) {
    return super.update(id, item);
  }

  delete(id: number) {
    return super.delete(id);
  }

  getAppointments(id: number): Observable<Appointment[]> {
    const url = `${this.getItemUrl(id)}/appointments`
    return this.restService.get<Appointment[]>(url).pipe(map(data => data.map(data => new Appointment(data))));
  }
  
}
