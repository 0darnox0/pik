import { Component, OnInit } from '@angular/core';

import { Appointment } from '../model/appointment';
import { AppointmentService } from '../services/appointment.service';
import { tap, catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {

  appointments: Appointment[];
  private loadingFailed: boolean = false;
  private loading: boolean = false;

  constructor(private userService: UserService, private authService: AuthService) { 
  }

  ngOnInit() {
    this.getAppointments()
  }

  getAppointments(): void {
    const user_id = this.authService.currentUserID;
    this.loading = true;
    this.userService.getAppointments(user_id).pipe(
      tap(() => {
        this.loadingFailed = false;
      }),
      catchError(() => {
        this.loadingFailed = true;
        return of([]);
      }),
      finalize(() => this.loading = false)
    ).subscribe((appointments: Appointment[]) => {
      this.appointments = this.sortAppointments(appointments);
    });
  }

  private sortAppointments(appointments: Appointment[]) {
    return appointments.sort((a: Appointment, b: Appointment) => {
      return a.date_start.getTime() - b.date_start.getTime();
    })
  }

}
