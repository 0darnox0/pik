import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeHoursFormComponent } from './office-hours-form.component';
import { MockComponent } from 'ng-mocks';
import { OfficeHoursTemplateComponent } from '../office-hours-template/office-hours-template.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

describe('OfficeHoursFormComponent', () => {
  let component: OfficeHoursFormComponent;
  let fixture: ComponentFixture<OfficeHoursFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        NgbDatepickerModule,
      ],
      declarations: [ 
        OfficeHoursFormComponent,
        MockComponent(OfficeHoursTemplateComponent),
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeHoursFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
