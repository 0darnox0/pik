import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { addDays, addMinutes, endOfDay } from 'date-fns';

@Component({
  selector: 'app-office-hours-form',
  templateUrl: './office-hours-form.component.html',
  styleUrls: ['./office-hours-form.component.scss'],
  providers: [{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}]
})
export class OfficeHoursFormComponent implements OnInit {
  fromDate: Date;
  toDate: Date;
  officeHoursForm: FormGroup;

  @Output() onSubmit: EventEmitter<any> = new EventEmitter();

  constructor(calendar: NgbCalendar, private formBuilder: FormBuilder) {
    this.officeHoursForm = new FormGroup({
      fromDate: new FormControl(new Date()),
      toDate: new FormControl(addDays(new Date(), 7)),
      officeHours: new FormControl(),
    }, { validators: this.dateRangeValidator });    
   } 

   dateRangeValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const fromDate = Date.parse(control.get('fromDate').value);
    const toDate = Date.parse(control.get('toDate').value);
    
    return fromDate && toDate && fromDate > toDate ? { 'dateRange': true } : null;
  };

  ngOnInit() {
  }

  formSubmitted() {
    this.onSubmit.emit(this.officeHoursForm.value);
  }

}
