import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Appointment } from '../model/appointment';
import { AppointmentService } from '../services/appointment.service';
import { tap, catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs';
 
@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.scss']
})
export class AppointmentDetailComponent implements OnInit {
  @Input() appointment: Appointment;
  loadingFailed: boolean = false;
  loading: boolean = false;
  
  constructor(private route: ActivatedRoute, 
              private appointmentService: AppointmentService, 
              private location: Location) { }
 
  ngOnInit() {
    this.getAppointment();
  }
  
  getAppointment(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.loading = true;
    this.appointmentService.get(id).pipe(
      tap(() => {
        this.loadingFailed = false;
      }),
      catchError(() => {
        this.loadingFailed = true;
        return of(null);
      }),
      finalize(() => this.loading = false)
    ).subscribe(appointment => this.appointment = appointment);
  }

  goBack(): void {
    this.location.back();
  }

  cancelAppointment():void {
    if(confirm("Are you sure you want to cancel this appointment?")) {
      const id = this.appointment.id;
      this.appointment.cancel();

      this.appointmentService.update(id, this.appointment).subscribe(
        err => {
          console.error(err)
      });
    }
  }

}