import { Component, OnInit, Input } from '@angular/core';
import { Patient } from 'src/app/model/patient';
import { PatientService } from 'src/app/services/patient.service';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';
import { catchError, tap, finalize } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @Input() user: User;
  loadingFailed: boolean = false;
  loading: boolean = false;
  savingFailed: boolean = false;
  saving: boolean = false;

  constructor(private userService: UserService, private authService: AuthService) { }

  ngOnInit() {
    const id = this.authService.currentUserID;
    this.loading = true;
    this.userService.get(id).pipe(
      tap(() => {
        this.loadingFailed = false;
      }),
      catchError(() => {
        this.loadingFailed = true;
        return of(null);
      }),
      finalize(() => this.loading = false)
    ).subscribe(user => {this.user = user; console.log(user)});
  }

  onSubmit(form) {
    const id = this.authService.currentUserID;
    this.saving = true;
    this.userService.update(id, this.user).pipe(
      tap(() => {
        this.savingFailed = false;
      }),
      finalize(() => this.saving = false)
    ).subscribe(
      data => {
        this.savingFailed = false;
      },
      err => {
        this.savingFailed = true;
        console.error(err);
      }
    )

  }
}