import { Component, OnInit, Injectable, Directive, forwardRef } from '@angular/core';
import { PatientService } from 'src/app/services/patient.service';
import { Patient } from 'src/app/model/patient';
import { AsyncValidator, AbstractControl, ValidationErrors, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class LoginValidator implements AsyncValidator {
  constructor(private userService: UserService) {}

  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.userService.getAllByLogin(ctrl.value).pipe(
      map(users => (users.length > 0 ? { uniqueLogin: true } : {})),
      catchError(() => of({}))
    );
  }
}

@Directive({
  selector: '[loginValidator]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => LoginValidator),
      multi: true
    }
  ]
})
export class LoginValidatorDirective {
  constructor(private validator: LoginValidator) {}

  validate(control: AbstractControl) {
    this.validator.validate(control);
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  
  patient: Patient = new Patient();
  saving = false;
  savingFailed = false;

  constructor(private patientService: PatientService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form) {
    this.savingFailed = false;
    this.saving = true;

    this.patientService.create(this.patient).subscribe(
      data => {
        this.saving = false;
        form.reset();
        this.router.navigate(['/login']);
      },
      err => {
        this.saving = false;
        this.savingFailed = true;
        console.error(err);
      },
    )

  }
}