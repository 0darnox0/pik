import { Component, OnInit } from '@angular/core';
import { of, Observable } from 'rxjs';
import { CalendarEvent } from 'calendar-utils';
import { map, filter } from 'rxjs/operators';
import { DoctorService } from '../services/doctor.service';
import { OfficeHour } from '../model/office-hour';
import { colors } from '../select-date/select-date.component';
import { Router } from '@angular/router';
import { CalendarView } from 'angular-calendar';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-schedule-view',
  templateUrl: './schedule-view.component.html',
  styleUrls: ['./schedule-view.component.scss'],
})
export class ScheduleViewComponent implements OnInit {
  CalendarView = CalendarView;

  scheduleEvents$: Observable<CalendarEvent[]>;
  clickedEvent: CalendarEvent;

  constructor(private doctorService: DoctorService, private router: Router, private authService: AuthService) {}

  ngOnInit() {
    const doctor_id = this.authService.currentUserID;
    this.scheduleEvents$ = this.doctorService.getOfficeHours(doctor_id).pipe(
      map( officeHours => officeHours.filter(officeHour => !officeHour.isPast())),
      map( officeHours => officeHours.map(officeHour => this.officeHour2CalendarEvent(officeHour)))
    )
  }

  onEventClicked(event: CalendarEvent) {
    if(event) {
      const id = event.meta.id;
      console.log(event);
      this.router.navigate(['/appointment', id])
    }
  }

  private officeHour2CalendarEvent(officeHour: OfficeHour): CalendarEvent {
    return {
      start: officeHour.start, 
      end: officeHour.end, 
      color: officeHour.isAvailable() ?  colors.green : colors.red,
      title: officeHour.occupied ? "Appointment" : "Free",
      meta: {
        available: !officeHour.isAvailable(),
        id: officeHour.appointment_id,
      }
    }
  }
}
