import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap} from 'rxjs/operators';
import { Doctor } from '../model/doctor';
import { DoctorService } from '../services/doctor.service';
import { NgbTypeaheadConfig } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent } from 'angular-calendar';
import { OfficeHour } from '../model/office-hour';
import { colors } from '../select-date/select-date.component';
import { Appointment } from '../model/appointment';
import { AppointmentService } from '../services/appointment.service';
import { Patient } from '../model/patient';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-make-appointment',
  templateUrl: './make-appointment.component.html',
  styleUrls: ['./make-appointment.component.scss'],
  providers: [NgbTypeaheadConfig]
})
export class MakeAppointmentComponent implements OnInit {
  selectedDoctor: Doctor;
  selectedEvent: CalendarEvent;

  searching = false;
  searchFailed = false;
  officeHoursEvents$: Observable<CalendarEvent[]>;

  saving = false;
  savingFailed = false;

  formatter = (result: Doctor) => result.fullName();

  constructor(private doctorService: DoctorService, 
              private appointmentService: AppointmentService, 
              private ngbConfig: NgbTypeaheadConfig, 
              private router: Router,
              private authService: AuthService) { 
    ngbConfig.editable = false;
  }

  ngOnInit() {
  }
  
  onSubmit(form) {
    let doctor = form.value.doctor;
    let event = form.value.event;
    let patient = new Patient(this.authService.currentUserValue);
    let appointment = new Appointment({
      doctor: doctor, 
      patient: patient, 
      date_start: event.start, 
      date_end: event.end
    });
    this.savingFailed = false;
    this.saving = true;
    this.appointmentService.create(appointment).subscribe(
      data => {
        this.router.navigate(['/appointments']);
      },
      err => {
        this.savingFailed = true;
        console.error(err);
      },
      () => {
        this.saving = false;
      }
    )

    form.reset();
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this.doctorService.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false)
    )
    
    selectedItem({ item }: { item: Doctor }) {
      let doctor = item;
      this.officeHoursEvents$ = this.doctorService.getOfficeHours(doctor.id).pipe(
        map( officeHours => officeHours.filter(officeHour => !officeHour.isPast())),
        map( officeHours => officeHours.map(officeHour => this.officeHour2CalendarEvent(officeHour)))
      )
    }

    private officeHour2CalendarEvent(officeHour: OfficeHour): CalendarEvent {
      return {
        start: officeHour.start, 
        end: officeHour.end, 
        color: officeHour.isAvailable() ? colors.green : colors.red,
        title: officeHour.isAvailable() ? "Available" : "Unavailable",
        meta: {
          available: officeHour.isAvailable(),
          id: officeHour.id,
        }
      }
    }
}
