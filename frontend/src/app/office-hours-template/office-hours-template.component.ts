import { Component, OnInit, ViewEncapsulation, Input, forwardRef, ChangeDetectorRef } from '@angular/core';
import { CalendarEvent, CalendarDateFormatter, DAYS_OF_WEEK, CalendarEventAction} from 'angular-calendar';
import { ControlValueAccessor, NG_VALUE_ACCESSOR  } from '@angular/forms';
import { CustomDateFormatter } from '../select-date/custom-date-formatter.provider';
import { DayViewHourSegment } from 'calendar-utils';
import { addDays, addMinutes, endOfDay } from 'date-fns';
import { fromEvent } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

function floorToNearest(amount: number, precision: number) {
  return Math.floor(amount / precision) * precision;
}

function ceilToNearest(amount: number, precision: number) {
  return Math.ceil(amount / precision) * precision;
}

@Component({
  selector: 'app-office-hours-template',
  templateUrl: './office-hours-template.component.html',
  styleUrls: ['./office-hours-template.component.scss'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    },
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OfficeHoursTemplateComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class OfficeHoursTemplateComponent implements OnInit, ControlValueAccessor {

  viewDate: Date = new Date();

  dayStartHour: number = 7;
  dayEndHour:   number = 17;
  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;

  @Input() events: CalendarEvent[] = [];

  dragToCreateActive = false;
  
  propagateChange = (_: any) => {};

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.deleteEvent(event);
      }
    }
  ];

  constructor(private cdr: ChangeDetectorRef) { }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
    this.propagateChange(this.events);
  }

  startDragToCreate(
    segment: DayViewHourSegment,
    mouseDownEvent: MouseEvent,
    segmentElement: HTMLElement
  ) {
    let overlapping = this.events.some((event) => {
        return event.start.getDate() == segment.date.getDate();
    })
    if(overlapping) return;
    
    const dragToSelectEvent: CalendarEvent = {
      id: this.events.length,
      title: '',
      start: segment.date,
      end: addMinutes(segment.date, 30),
      actions: this.actions,
      meta: {
        tmpEvent: true
      },
    };
    dragToSelectEvent.title = this.titleFormatter(dragToSelectEvent);
    this.events = [...this.events, dragToSelectEvent];
    const segmentPosition = segmentElement.getBoundingClientRect();
    this.dragToCreateActive = true;

    const endOfView = endOfDay(segment.date);

    fromEvent(document, 'mousemove')
      .pipe(
        finalize(() => {
          delete dragToSelectEvent.meta.tmpEvent;
          this.dragToCreateActive = false;
          this.refresh();
        }),
        takeUntil(fromEvent(document, 'mouseup'))
      )
      .subscribe((mouseMoveEvent: MouseEvent) => {
        const minutesDiff = ceilToNearest(
          mouseMoveEvent.clientY - segmentPosition.top,
          30
        );

        const daysDiff =
          floorToNearest(
            mouseMoveEvent.clientX - segmentPosition.left,
            segmentPosition.width
          ) / segmentPosition.width;

        const newEnd = addDays(addMinutes(segment.date, minutesDiff), daysDiff);
        if (newEnd > segment.date && newEnd < endOfView) {
          dragToSelectEvent.end = newEnd;
        }
        dragToSelectEvent.title = this.titleFormatter(dragToSelectEvent);
        this.refresh();
        this.propagateChange(this.events);
      });
  }

  private titleFormatter(event: CalendarEvent): string {
    return `<b>
              <i class='far fa-clock'></i>
              ${new DatePipe('en').transform(event.start, 'HH:mm')} - 
              ${new DatePipe('en').transform(event.end, 'HH:mm')}
          </b> `
  }

  private refresh() {
    this.events = [...this.events];
    this.cdr.detectChanges();
  }

  ngOnInit() {}

  writeValue(value: any) {
    if (value) {
      this.events = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

}
