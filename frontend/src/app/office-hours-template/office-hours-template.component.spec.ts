import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeHoursTemplateComponent } from './office-hours-template.component';
import { CalendarModule, DateAdapter, CalendarUtils } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

describe('OfficeHoursTemplateComponent', () => {
  let component: OfficeHoursTemplateComponent;
  let fixture: ComponentFixture<OfficeHoursTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CalendarModule.forRoot({
          provide: DateAdapter,
          useFactory: adapterFactory
        })
      ],
      declarations: [ 
        OfficeHoursTemplateComponent,
       ],
       providers: [
        CalendarUtils
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeHoursTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
