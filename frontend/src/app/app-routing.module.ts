import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { AppointmentDetailComponent }  from './appointment-detail/appointment-detail.component';
import { MakeAppointmentComponent } from './make-appointment/make-appointment.component';
import { OfficeHoursCreateComponent } from './office-hours-create/office-hours-create.component';
import { ScheduleViewComponent } from './schedule-view/schedule-view.component';
import { AuthGuard } from './guards/auth.guard';
import { Role } from './model/role';

const routes: Routes = [
  { path: 'schedule', component: ScheduleViewComponent, canActivate: [AuthGuard], data: { roles: [Role.Doctor] } },
  { path: 'office-hours/create', component: OfficeHoursCreateComponent, canActivate: [AuthGuard], data: { roles: [Role.Doctor] }},
  { path: 'appointment/make', component: MakeAppointmentComponent, canActivate: [AuthGuard], data: { roles: [Role.Patient] }},
  { path: 'appointment/:id', component: AppointmentDetailComponent, canActivate: [AuthGuard], data: { roles: [Role.Patient, Role.Doctor] }},
  { path: 'appointments', component: AppointmentsComponent, canActivate: [AuthGuard], data: { roles: [Role.Patient] }},
  { path: '', component: HomepageComponent, pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
