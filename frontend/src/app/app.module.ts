import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { HomepageComponent } from './homepage/homepage.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthModule } from './auth/auth.module';
import { AppointmentsComponent } from './appointments/appointments.component';
import { AppointmentDetailComponent } from './appointment-detail/appointment-detail.component';
import { FormsModule, FormBuilder } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { MakeAppointmentComponent } from './make-appointment/make-appointment.component';
import { SelectDateComponent } from './select-date/select-date.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbTypeaheadModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { OfficeHoursTemplateComponent } from './office-hours-template/office-hours-template.component';
import { OfficeHoursFormComponent } from './office-hours-form/office-hours-form.component';
import { OfficeHoursCreateComponent } from './office-hours-create/office-hours-create.component';
import { ScheduleViewComponent } from './schedule-view/schedule-view.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PageNotFoundComponent,
    AppointmentsComponent,
    AppointmentDetailComponent,
    MakeAppointmentComponent,
    SelectDateComponent,
    OfficeHoursTemplateComponent,
    OfficeHoursFormComponent,
    OfficeHoursCreateComponent,
    ScheduleViewComponent,
  ],
  imports: [
    BrowserModule,
    AuthModule,
    AppRoutingModule,
    UiModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbTypeaheadModule,
    NgbDatepickerModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
