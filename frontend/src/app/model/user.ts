import { Role } from './role';

export class User {
    id: number;
    first_name: string;
    last_name: string;
    login: string;
    password: string;
    role: Role;
    token?: string;
  
    constructor(item: any = {}){
      Object.assign(this, item);
    }

    fullName():string {
      return this.first_name + " " + this.last_name;
    }
  }
