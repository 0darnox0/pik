
export class WeeklySchedule {
    startOfValidity: Date;
    endOfValidity: Date;
    schedule: WorkingDay[];

    constructor(item: any){
        Object.assign(this, item);

        this.startOfValidity = new Date(item.startOfValidity);
        this.endOfValidity = new Date(item.endOfValidity);
        this.schedule = item.schedule.array.forEach((day: WorkingDay) => {
            return new WorkingDay(day);
        });
    }
}
  
export class WorkingDay {
    week_day: number;
    start_hour: number;
    start_minutes: number;
    end_hour: number;
    end_minutes: number;

    constructor(item: any){
        Object.assign(this, item);
    }
}