import { User } from './user';

export class Doctor extends User {
    specialization: string;
  
    constructor(item: any){
      super(item);
      Object.assign(this, item);
    }
  }
