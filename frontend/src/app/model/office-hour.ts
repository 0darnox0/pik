import { Doctor } from './doctor';

export class OfficeHour {
  id: number;
  doctor: Doctor;
  start: Date;
  end: Date;
  occupied: boolean;
  appointment_id: number;

  constructor(item: any){
    Object.assign(this, item);

    this.doctor = new Doctor(item.doctor);
    this.start = new Date(item.start);
    this.end = new Date(item.end);
  }

  isPast():boolean {
    return this.start <= new Date();
  }

  isAvailable():boolean {
    return !this.isPast() && !this.occupied;
  }
}
