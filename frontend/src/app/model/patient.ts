import { User } from './user';

export class Patient extends User {
    pesel: string;
  
    constructor(item: any = {}){
      super(item);
      Object.assign(this, item);
    }
  }
