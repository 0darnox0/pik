import { Doctor } from './doctor';
import { Patient } from './patient';
import '../utils/Date';

export class Appointment {
  id: number;
  doctor: Doctor;
  patient: Patient;
  date_start: Date;
  date_end: Date;
  room: string;
  canceled: boolean;

  constructor(item: any){
    Object.assign(this, item);
    
    this.doctor = new Doctor(item.doctor);
    this.patient = new Patient(item.patient);
    this.date_end = new Date(item.date_end);
    this.date_start = new Date(item.date_start);
  }

  isPast():boolean {
    return this.date_start <= new Date();
  }

  canBeCanceled():boolean {
    return !this.isPast() && !this.canceled;
  }

  cancel():void {
    this.canceled = true;
  }

}
