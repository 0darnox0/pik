import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { CalendarEvent, CalendarView, CalendarDateFormatter, DAYS_OF_WEEK, CalendarEventTitleFormatter, 
         CalendarMonthViewBeforeRenderEvent } from 'angular-calendar';
import { CustomDateFormatter } from './custom-date-formatter.provider';
import { Observable } from 'rxjs';
import { CustomEventTitleFormatter } from './custom-event-title-formatter.provider';
import { ControlValueAccessor, NG_VALUE_ACCESSOR  } from '@angular/forms';

export const colors = {
  red: {
    primary: "#b94848",
    secondary: "#fde7e7",
  },
  green: {
    primary: "#51b948",
    secondary: "#e8fde7",
  }
};

@Component({
  selector: 'app-select-date',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    },
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter
    },
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectDateComponent),
      multi: true
    }
  ]
})
export class SelectDateComponent implements OnInit, ControlValueAccessor  {
  CalendarView=CalendarView;
  @Input() view: CalendarView = CalendarView.Month;
  viewDate: Date = new Date();

  dayStartHour: number = 7;
  dayEndHour:   number = 17;
  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;
  
  @Input() events$: Observable<CalendarEvent[]>;
  selectedDate: CalendarEvent;

  propagateChange = (_: any) => {};

  constructor() { }

  ngOnInit() { }

  writeValue(value: any) {
    if (value) {
      this.selectedDate = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  eventClicked({ event }: { event: CalendarEvent }): void {
    if(!event.meta.available) return;

    if (this.selectedDate != undefined) {
      this.selectedDate.cssClass = '';
    }

    if (event !== this.selectedDate) {
      event.cssClass = 'selected-date';
      this.selectedDate = event;
    } else {
      this.selectedDate = undefined;
    } 

    this.propagateChange(this.selectedDate);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
    renderEvent.body.forEach(day => {
      day.badgeTotal = day.events.filter(
        event => event.meta.available
      ).length;
    });
  }
}
