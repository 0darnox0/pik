import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';
import { DatePipe } from '@angular/common';

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  week(event: CalendarEvent): string {
    return `
        <b>
            <i class='far fa-clock'></i>
            ${new DatePipe('en').transform(event.start, 'HH:mm')} - 
            ${new DatePipe('en').transform(event.end, 'HH:mm')}
        </b> 
        ${event.title}`;
  }
}
