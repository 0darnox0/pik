import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeHoursCreateComponent } from './office-hours-create.component';
import { OfficeHoursFormComponent } from '../office-hours-form/office-hours-form.component';
import { MockComponent } from 'ng-mocks';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('OfficeHoursCreateComponent', () => {
  let component: OfficeHoursCreateComponent;
  let fixture: ComponentFixture<OfficeHoursCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ 
        OfficeHoursCreateComponent,
        MockComponent(OfficeHoursFormComponent)
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeHoursCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
