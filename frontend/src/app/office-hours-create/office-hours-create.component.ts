import { Component, OnInit } from '@angular/core';
import { CalendarEvent } from 'calendar-utils';
import { WeeklySchedule, WorkingDay } from '../model/weekly-schedule'
import { DoctorService } from '../services/doctor.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-office-hours-create',
  templateUrl: './office-hours-create.component.html',
  styleUrls: ['./office-hours-create.component.scss']
})
export class OfficeHoursCreateComponent implements OnInit {

  saving = false;
  savingFailed = false;

  constructor(private doctorService: DoctorService, private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit($event: any) {
    if (!$event) return;

    let fromDate = $event.fromDate as Date;
    let toDate = $event.toDate as Date;
    let officeHours = $event.officeHours as CalendarEvent[];
    
    fromDate.setHours(0);
    fromDate.setMinutes(0);
    fromDate.setSeconds(0);
    
    toDate.setHours(23);
    toDate.setMinutes(59);
    toDate.setSeconds(59);

    const result: WeeklySchedule = {
        startOfValidity: fromDate,
        endOfValidity: toDate,
        schedule: officeHours.map((event) => {
          return this.event2WorkingDay(event);
        })
    }

    this.savingFailed = false;
    this.saving = true;
    const doctorId = this.authService.currentUserValue.id;
    this.doctorService.createWeeklySchedule(doctorId, result).subscribe(
      data => {
        this.saving = false;
      },
      err => {
        this.savingFailed = true;
        this.saving = false;
        console.error(err);
      }
    )
  }

  event2WorkingDay(event: any): WorkingDay {
    return {
      week_day: event.start.getDay() || 7,
      start_hour: event.start.getHours(), 
      start_minutes: event.start.getMinutes(),
      end_hour: event.end.getHours(), 
      end_minutes: event.end.getMinutes(),
    };
  }
}


