import * as moment from 'moment';

export {}

declare global {
    interface Date {
        toJSON(): string;
    }
}

Date.prototype.toJSON = function (): string {
    return moment(this).format('YYYY-MM-DD[T]HH:mm:ssZZ');
}